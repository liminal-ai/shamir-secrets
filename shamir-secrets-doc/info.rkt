#lang info

(define version "0.1")

(define collection 'multi)

(define deps
  '())

(define build-deps
  '("base"
    "racket-doc"
    "scribble-lib"
    "shamir-secrets-lib"))