#lang scribble/manual

@(require (for-label shamir-secrets))

@title{shamir-secrets}

@author{liminal-enterprises}

@defmodule[shamir-secrets]

An automagically generated Typed Racket package