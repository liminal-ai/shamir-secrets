shamir-secrets
====

[![Build Status](https://gitlab.com/liminal-enterprises/shamir-secrets/badges/master/build.svg)](https://gitlab.com/liminal-enterprises/shamir-secrets/commits/master) [![codecov](https://codecov.io/gl/liminal-enterprises/shamir-secrets/branch/master/graph/badge.svg)](https://codecov.io/gl/liminal-enterprises/shamir-secrets)

A library for Shamir's Secret Sharing in Racket

Includes share structs, and encoding/decoding to plain bits

Supports up to 65k shares currently; if you need more than that,
you're _probably_ doing something weird.
